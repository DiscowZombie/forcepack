package fr.discowzombie.forcepack.service;

import fr.discowzombie.forcepack.ForcePack;
import fr.discowzombie.forcepack.utils.PackUtils;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;
import java.util.HexFormat;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;

public class RessourcePackService {

    @NotNull
    private final String url;
    @NotNull
    private final JavaPlugin plugin;
    @Nullable
    private BukkitTask refreshTask = null;

    @NotNull
    private byte[] currentHash = new byte[0];
    @Nullable
    private Date lastCheck = null;

    public RessourcePackService(final @NotNull String url) {
        this.url = url;
        this.plugin = ForcePack.getPlugin(ForcePack.class);
    }

    public boolean startScheduling() {
        if (this.refreshTask != null)
            return false;
        this.refreshTask = Bukkit.getScheduler().runTaskTimerAsynchronously(this.plugin, () -> {
            try {
                final var now = new Date();
                final var hash = PackUtils.computeSha1Hash(this.url, this.lastCheck).get(10, TimeUnit.SECONDS);

                if (hash.length == 0) {
                    this.lastCheck = now;
                    return;
                }

                this.currentHash = hash;
                this.lastCheck = now;
                this.plugin.getLogger().info("New ressource-pack update installed (hash: " + HexFormat.of().formatHex(this.currentHash) + ")");
            } catch (ExecutionException | InterruptedException | TimeoutException e) {
                this.plugin.getLogger().log(Level.WARNING, "Unable to compute ressource-pack hash", e);
            }
        }, 0L, 30 * 20L);
        return true;
    }

    public boolean stopScheduling() {
        if (this.refreshTask != null) {
            this.refreshTask.cancel();
            this.refreshTask = null;
            return true;
        }
        return false;
    }

    @NotNull
    public Optional<byte[]> getHash() {
        return (this.currentHash.length == 0) ? Optional.empty() : Optional.of(this.currentHash);
    }

    @NotNull
    public String getUrl() {
        return this.url;
    }

    /**
     * URL with the hash appended, to fix <a href="https://bugs.mojang.com/browse/MC-164316">MC-164316</a>
     *
     * @return The pack url with the sha appended
     */
    @NotNull
    public String getHashedUrl() {
        if (this.currentHash.length == 0) {
            return this.url;
        } else {
            return String.format("%s#%s", this.url, HexFormat.of().formatHex(this.currentHash));
        }
    }
}
