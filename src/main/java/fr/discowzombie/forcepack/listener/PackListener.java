package fr.discowzombie.forcepack.listener;

import fr.discowzombie.forcepack.service.RessourcePackService;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.event.ClickEvent;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerResourcePackStatusEvent;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

public final class PackListener implements Listener {

    @NotNull
    private final RessourcePackService ressourcePackService;

    public PackListener(@NotNull final RessourcePackService ressourcePackService) {
        this.ressourcePackService = ressourcePackService;
    }

    @EventHandler
    private void onJoin(final PlayerJoinEvent event) {
        final Player player = event.getPlayer();
        final Optional<byte[]> hashOpt = this.ressourcePackService.getHash();

        player.setResourcePack(
                this.ressourcePackService.getHashedUrl(),
                hashOpt.orElse(new byte[0]),
                Component.text("Pour une expérience de jeu optimale, veuillez accepter le pack de ressources.", NamedTextColor.GOLD),
                true
        );
    }

    @EventHandler
    private void onRejectPack(PlayerResourcePackStatusEvent event) {
        final var status = event.getStatus();
        final var player = event.getPlayer();

        if (status == PlayerResourcePackStatusEvent.Status.DECLINED ||
                status == PlayerResourcePackStatusEvent.Status.FAILED_DOWNLOAD) {
            player.sendMessage(
                    Component.text("Le ressource-pack n'a pas été installé. Utilisez la commande", NamedTextColor.GRAY)
                            .append(Component.text(" /pack ", NamedTextColor.WHITE).clickEvent(ClickEvent.runCommand("/pack")))
                            .append(Component.text("pour l'obtenir.", NamedTextColor.GRAY))
            );
        }
    }
}
