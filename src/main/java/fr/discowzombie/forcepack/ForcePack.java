package fr.discowzombie.forcepack;

import fr.discowzombie.forcepack.command.PackCommand;
import fr.discowzombie.forcepack.listener.PackListener;
import fr.discowzombie.forcepack.service.RessourcePackService;
import org.bukkit.Bukkit;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;
import org.codehaus.plexus.util.IOUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.CheckReturnValue;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Objects;
import java.util.Optional;

public final class ForcePack extends JavaPlugin {

    @Nullable
    private RessourcePackService ressourcePackService = null;

    @NotNull
    private String readUrlFromFile(@NotNull File destFile) throws IOException {
        try (final Reader reader = new FileReader(destFile)) {
            final var fullString = IOUtil.toString(reader);
            return fullString.strip();
        }
    }

    @Override
    public void onEnable() {
        final var packUrlOpt = this.readPackUrl();

        if (packUrlOpt.isPresent()) {
            this.getLogger().info("Ressource-pack URL is: " + packUrlOpt.get());

            this.ressourcePackService = new RessourcePackService(packUrlOpt.get());
            this.ressourcePackService.startScheduling();

            Bukkit.getServicesManager().register(RessourcePackService.class, this.ressourcePackService, this, ServicePriority.Normal);

            Bukkit.getPluginManager().registerEvents(new PackListener(this.ressourcePackService), this);

            Objects.requireNonNull(getCommand("pack")).setExecutor(new PackCommand(this.ressourcePackService));
        } else {
            this.getLogger().info("No ressource-pack available.");
        }
    }

    @Override
    public void onDisable() {
        if (this.ressourcePackService != null) {
            this.ressourcePackService.stopScheduling();
            this.ressourcePackService = null;
        }
    }

    // TODO patch le mauvais pack envoyé

    @NotNull
    @CheckReturnValue
    private Optional<String> readPackUrl() {
        // Try to read from env
        var packUrl = System.getenv("PACK_URL");
        if (packUrl != null && !packUrl.isBlank()) {
            return Optional.of(packUrl);
        }

        // Try to read from file
        else {
            // Create the plugin data folder
            this.getDataFolder().mkdirs();

            final var packUrlFile = new File(this.getDataFolder(), "pack_url");

            try {
                packUrl = this.readUrlFromFile(packUrlFile);
                if (!packUrl.isBlank()) {
                    return Optional.of(packUrl);
                }
            } catch (IOException ignored) {
            }
        }

        return Optional.ofNullable(packUrl);
    }
}
