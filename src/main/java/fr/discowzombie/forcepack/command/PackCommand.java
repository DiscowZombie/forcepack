package fr.discowzombie.forcepack.command;

import fr.discowzombie.forcepack.service.RessourcePackService;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

public final class PackCommand implements CommandExecutor {

    @NotNull
    private final RessourcePackService ressourcePackService;

    public PackCommand(@NotNull final RessourcePackService ressourcePackService) {
        this.ressourcePackService = ressourcePackService;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (sender instanceof final Player player) {
            final Optional<byte[]> hashOpt = this.ressourcePackService.getHash();

            player.setResourcePack(
                    this.ressourcePackService.getHashedUrl(),
                    hashOpt.orElse(new byte[0]),
                    Component.text("Pour une expérience de jeu optimale, veuillez accepter le pack de ressources.", NamedTextColor.GOLD),
                    true
            );
        }

        return true;
    }
}
