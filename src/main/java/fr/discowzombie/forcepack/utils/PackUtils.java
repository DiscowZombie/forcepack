package fr.discowzombie.forcepack.utils;

import org.jetbrains.annotations.CheckReturnValue;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;

public final class PackUtils {

    private static final HttpClient HTTP_CLIENT = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_1_1) // For some reason, HTTP/2 keep streams opened "too many concurrent streams"
            .build();

    private static final SimpleDateFormat RFC822_DATE_FORMAT = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH);

    private static final MessageDigest SHA_1_DIGEST;

    static {
        try {
            SHA_1_DIGEST = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    private PackUtils() {
    }

    @CheckReturnValue
    @NotNull
    public static CompletableFuture<byte[]> computeSha1Hash(final @NotNull String url, final @Nullable Date previousCheck) {
        final var request = HttpRequest.newBuilder(URI.create(url))
                .GET()
                //.header("If-Modified-Since", RFC822_DATE_FORMAT.format(new Date())) // Scaleway does not support it.
                .build();

        return HTTP_CLIENT.sendAsync(request, HttpResponse.BodyHandlers.ofInputStream())
                .thenApplyAsync(response -> {
                    try {
                        if (response.statusCode() != 200) { // Failed :(
                            throw new CompletionException(new Exception("Bad HTTP code returned: " + response.statusCode()));
                        }

                        final var lastModifiedHeaderOpt = response.headers().firstValue("last-modified");
                        if (previousCheck != null &&
                                lastModifiedHeaderOpt.isPresent() &&
                                RFC822_DATE_FORMAT.parse(lastModifiedHeaderOpt.get()).before(previousCheck)) {
                            // No change since the last time
                            return new byte[0];
                        }

                        try (final var inputStream = response.body()) {
                            final byte[] b = inputStream.readAllBytes();
                            return SHA_1_DIGEST.digest(b);
                        }
                    } catch (ParseException | IOException e) {
                        throw new CompletionException(e);
                    }
                });
    }
}
